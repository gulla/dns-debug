FROM ubuntu:xenial

RUN apt-get update && \
  apt-get install -y \
  dnsutils \
  tmux \
  && rm -rf /var/lib/apt/lists/*

